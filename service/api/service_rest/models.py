from django.db import models


# Create your models here.
class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=50)


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=50)
    sold = models.BooleanField(default=False)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=100)
    cancelled = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)
    vin = models.CharField(max_length=50)
    vip = models.BooleanField(default=False)
    customer = models.CharField(max_length=50)
    technician = models.ForeignKey(Technician, on_delete=models.CASCADE, null=True)
