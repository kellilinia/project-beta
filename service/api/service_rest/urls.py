from django.urls import path
from .views import get_technicians

urlpatterns = [
    path("technicians/", get_technicians, name="get_technicians"),
]
