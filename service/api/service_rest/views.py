from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .models import Technician, Appointment
from common.json import ModelEncoder


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name", "last_name", "employee_id", "id"]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "cancelled",
        "finished",
        "vin",
        "vip",
        "customer",
        "technician",
        "id",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }


@require_http_methods(["GET", "POST"])
def get_technicians(request):
    if request.method == "GET":
        try:
            technicians = Technician.objects.all()
            technicians_list = list(
                technicians.values("first_name", "last_name", "employee_id", "id")
            )
            return JsonResponse({"technicians": technicians_list})
        except Technician.DoesNotExist:
            return JsonResponse({"technicians": []})
    elif request.method == "POST":
        try:
            data = json.loads(request.body)
            technician = Technician.objects.create(
                first_name=data["first_name"],
                last_name=data["last_name"],
                employee_id=data["employee_id"],
            )
            return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)
        except KeyError:
            return JsonResponse({"error": "Invalid request data."})

    else:
        return JsonResponse({"error": "Invalid request method."})


@require_http_methods(["GET", "DELETE"])
def show_technician(request, id):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=id)
            return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)
        except Technician.DoesNotExist:
            return JsonResponse({"error": "Technician not found."})
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=id)
            technician.delete()
            return JsonResponse({"success": "Technician deleted."})
        except Technician.DoesNotExist:
            return JsonResponse({"error": "Technician not found."})
    else:
        return JsonResponse({"error": "Invalid request method."})


@require_http_methods(["GET", "POST"])
def get_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        if appointments:
            appointments_list = list(
                appointments.values(
                    "date_time",
                    "reason",
                    "cancelled",
                    "finished",
                    "vin",
                    "vip",
                    "customer",
                    "technician",
                    "id",
                )
            )
            return JsonResponse({"appointments": appointments_list})
        else:
            return JsonResponse({"appointments": []})
    elif request.method == "POST":
        try:
            data = json.loads(request.body)
            print("Data:", data)
            technician_id = data["technician"]
            technician = Technician.objects.get(id=technician_id)
        except Technician.DoesNotExist:
            return JsonResponse({"error": "Technician not found."})

        try:
            appointment = Appointment.objects.create(
                date_time=data["date_time"],
                reason=data["reason"],
                cancelled=False,
                finished=False,
                vin=data["vin"],
                customer=data["customer"],
                technician=technician,
            )
            print("Appointment:", appointment.__dict__)
            return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)
        except KeyError:
            return JsonResponse({"error": "Invalid request data."})
    else:
        return JsonResponse({"error": "Invalid request method."})


@require_http_methods(["GET", "PUT"])
def cancel_appointment(request, id):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)
        except Appointment.DoesNotExist:
            return JsonResponse({"error": "Appointment not found."})
    elif request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.cancelled = True
            appointment.save()
            return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)
        except Appointment.DoesNotExist:
            return JsonResponse({"error": "Appointment not found."})


@require_http_methods(["GET", "PUT"])
def finish_appointment(request, id):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)
        except Appointment.DoesNotExist:
            return JsonResponse({"error": "Appointment not found."})
    elif request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.finished = True
            appointment.save()
            return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)
        except Appointment.DoesNotExist:
            return JsonResponse({"error": "Appointment not found."})
    else:
        return JsonResponse({"error": "Invalid request method."})
