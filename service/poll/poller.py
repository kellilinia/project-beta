import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

from service_rest.models import AutomobileVO


def poll(repeat=True):
    while True:
        print("Service poller polling for data")
        try:
            # Write your polling logic, here
            response = requests.get(
                "http://project-beta-inventory-api-1:8000/api/automobiles/"
            )
            data = response.json()
            print(type(data))
            print(data)

            for automobile in data["autos"]:
                AutomobileVO.objects.update_or_create(
                    vin=automobile["vin"],
                    defaults={"vin": automobile["vin"], "sold": automobile["sold"]},
                )

                all_vos = AutomobileVO.objects.all()
            print("AutomobileVOs:")
            for vo in all_vos:
                print(vo)

        except Exception as e:
            print(e, file=sys.stderr)

        if not repeat:
            break

        time.sleep(60)


if __name__ == "__main__":
    poll()
