import React, { useState, useEffect } from 'react';

function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [vinFilter, setVinFilter] = useState('');

    const appointmentStatus = (appointment) => {
        if (appointment.cancelled) {
            return "Cancelled";
        } else if (appointment.finished) {
            return "Finished";
        } else {
            return "Scheduled";

        }
    }

        async function getAppointments() {
            try {
                const response = await fetch('http://localhost:8080/api/appointments/');
                if (response.ok) {
                    const jsonResponse = await response.json();
                    setAppointments(jsonResponse.appointments);
                } else {
                    console.log('Failed to retrieve appointments');
                }
            } catch (error) {
                console.log('Error:', error);
            }
        }
        useEffect(() => {
            getAppointments();
        }
            , []);
        
    
        return (
            <>
                <div className="form-group">
                    <label htmlFor="vinFilter">Filter by VIN:</label>
                    <input
                        type="text"
                        id="vinFilter"
                        className="form-control"
                        value={vinFilter}
                        onChange={e => setVinFilter(e.target.value)}
                    />
                </div>
                <div className="container mt-5">
                    <h1>Service History</h1>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Customer</th>
                                <th scope="col">Date/Time</th>
                                <th scope="col">Technician</th>
                                <th scope="col">VIN</th>
                                <th scope="col">Reason</th>
                                <th scope="col">Status</th>
                                <th scope="col">VIP</th>
                            </tr>
                        </thead>
                        <tbody>
                            {appointments.filter(appointment => appointment.vin.toLowerCase().includes(vinFilter.toLowerCase())).map((appointment, index) => (
                                <tr key={index}>
                                    <td>{appointment.customer}</td>
                                    <td>{appointment.date_time}</td>
                                    <td>{appointment.technician}</td>
                                    <td>{appointment.vin}</td>
                                    <td>{appointment.reason}</td>
                                    <td>{appointmentStatus(appointment)}</td>
                                    <td>{appointment.vip ? 'Yes' : 'No'}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </>

        );
    }


export default ServiceHistory;