function SalespeopleList({ salespeople }) {
    return (
        <table className="table table-striped">
            <thead>
                <h1>Salespeople</h1>
                <tr>
                    <th scope="col">Employee ID</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                </tr>
            </thead>
            <tbody>
                {salespeople && salespeople.map(salesperson => {
                    return (
                        <tr key={salesperson.id}>
                            <td>{salesperson.employee_id}</td>
                            <td>{salesperson.first_name}</td>
                            <td>{salesperson.last_name}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    )
}
export default SalespeopleList
