import React, { useState, useEffect } from 'react';

function CreateTechnician() {
    const [technician, setTechnician] = useState({});
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [employeeID, setEmployeeID] = useState("");
    
    useEffect(() => {
}, [technician]);

    const handleFirstNameChange = (event) => {
        setFirstName(event.target.value);
    };

    const handleLastNameChange = (event) => {
        setLastName(event.target.value);
    };

    const handleEmployeeIDChange = (event) => {
        setEmployeeID(event.target.value);
    };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const newTechnician = { first_name: firstName, last_name: lastName, employee_id: employeeID };
  
  
    try {
        const response = await fetch('http://localhost:8080/api/technicians/', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
            },
        body: JSON.stringify(newTechnician)
        });
    
        if (response.ok) {
        const jsonResponse = await response.json();
        setTechnician(jsonResponse);
    }   else {
        console.log('Failed to create technician');
    }
  } catch (error) {
    console.log('Error:', error);
  }
};


    return (
<>
    <div className='container'>
        <h1>Create Technician</h1>
        <form onSubmit={handleSubmit} className='mt-3'> 
            <div className='row'>
                <div className='col-lg-4'>
                    <div className='form-group'>
                        <label htmlFor='firstName'>First Name</label>
                        <input type='text' id='firstName' name='firstName' className='form-control' value={firstName} onChange={handleFirstNameChange} />
                    </div>
                    <div className='form-group mt-2'>
                        <label htmlFor='lastName'>Last Name</label>
                        <input type='text' id='lastName' name='lastName' className='form-control' value={lastName} onChange={handleLastNameChange} />
                    </div>
                    <div className='form-group mt-2'>
                        <label htmlFor='employeeID'>Employee ID</label>
                        <input type='text' id='employeeID' name='employeeID' className='form-control' value={employeeID} onChange={handleEmployeeIDChange} />
                    </div>
                    <button type='submit' className='btn btn-primary mt-3'>Create Technician</button>
                </div>
            </div>
        </form>
    </div>
</>


    )
}

export default CreateTechnician;
