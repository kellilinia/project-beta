import React, { useState, useEffect } from 'react';

function TechnicianList() {
    const [technicians, setTechnicians] = useState([]);

    async function getTechnicians() {
        try {
            const response = await fetch('http://localhost:8080/api/technicians/');
            if (response.ok) {
                const jsonResponse = await response.json();
                setTechnicians(jsonResponse.technicians);
            } else {
                console.log('Failed to retrieve technicians');
            }
        } catch (error) {
            console.log('Error:', error);
        }
    }
    useEffect(() => {
        getTechnicians();
    }
        , []);
    
    return (
        <>
            <div className="container mt-5">
                <h1>Technicians</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">First Name</th>
                            <th scope="col">Last Name</th>
                            <th scope="col">Employee ID</th>
                        </tr>
                    </thead>
                    <tbody>
                        {technicians.map((technician, index) => (
                            <tr key={index}>
                                <td>{technician.first_name}</td>
                                <td>{technician.last_name}</td>
                                <td>{technician.employee_id}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </>
    );
}
export default TechnicianList;