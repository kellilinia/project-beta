
import React, { useEffect, useState } from 'react';

function SalespersonHistory(props) {
    const [salesperson, setSalesperson] = useState('');
    const [salespeople, setSalespeople] = useState([]);
    const [sale, setSale] = useState('');
    const [sales, setSales] = useState([]);




    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();


        const data = {};
        data.salesperson = salesperson;
        data.sale = sale;
        console.log(data);

        const salespersonUrl = 'http://localhost:8090/api/salesespeople/';
        const fetchSalespersonConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const responseSalesperson = await fetch(salespersonUrl, fetchSalespersonConfig);
        if (responseSalesperson.ok) {
            const newSalesperson = await responseSalesperson.json();
            console.log(newSalesperson);
            setSalesperson('');

            props.getSalesperson();
        }


        const saleUrl = 'http://localhost:8090/api/sales/';
        const fetchSaleConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };



        const responseSale = await fetch(saleUrl, fetchSaleConfig);
        if (responseSale.ok) {
            const newSale = await responseSale.json();
            console.log(newSale);
            setSale('');

            props.getSale();
        }
    }

    const fetchSalespersonData = async () => {
        const url = 'http://localhost:8090/api/salespeople/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    }


    const fetchSalesData = async () => {
        const url = 'http://localhost:8090/api/sales/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    }

    useEffect(() => {
        fetchSalespersonData();
        fetchSalesData();
    }, []);

    return (
        <div>
            <h1>Salesperson History</h1>
            <div className="mb-3">
                <select onChange={handleSalespersonChange} required name="salesperson" id="salesperson" className="form-select">
                    <option value="">Choose a salesperson</option>
                    {salespeople.map(salesperson => (
                        <option key={salesperson.id} value={salesperson.employee_id}>
                            {salesperson.first_name} {salesperson.last_name}
                        </option>
                    ))}
                </select>
            </div>
            <div className="table-responsive">
                <table className="table table-striped">

                    <thead>
                        <tr>
                            <th scope="col">Salesperson</th>
                            <th scope="col">Customer</th>
                            <th scope="col">VIN</th>
                            <th scope="col">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {sales.filter(sale => sale.salesperson.id).map(sale => (
                            <tr key={sale.id}>
                                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>{sale.price}</td>
                            </tr>

                        ))}
                    </tbody>
                </table >
            </div>
        </div>
    )
}
export default SalespersonHistory
