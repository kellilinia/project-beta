import React, { useState, useEffect } from 'react';

function ListAppointments() {
    const [appointments, setAppointments] = useState([]);

    const cancelAppointment = async (id) => {
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            if (response.ok) {
                getAppointments();
            } else {
                console.log('Failed to cancel appointment');
            }
        } catch (error) {
            console.log('Error:', error);
        }
    }

    const finishAppointment = async (id) => {
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            if (response.ok) {
                getAppointments();
            } else {
                console.log('Failed to finish appointment');
            }
        } catch (error) {
            console.log('Error:', error);
        }
    }
    
    
    
    async function getAppointments() {
        try {
            const response = await fetch('http://localhost:8080/api/appointments/');
            if (response.ok) {
                const jsonResponse = await response.json();
                setAppointments(jsonResponse.appointments);
            } else {
                console.log('Failed to retrieve appointments');
            }
        } catch (error) {
            console.log('Error:', error);
        }
    }
    useEffect(() => {
        getAppointments();
    }
        , []);
    
    return (

    <>
        <div className="container mt-5">
            <h1>Appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Customer</th>
                        <th scope="col">Date/Time</th>
                        <th scope="col">Technician</th>
                        <th scope="col">VIN</th>
                        <th scope="col">Reason</th>
                        <th scope="col">Status</th>
                        <th scope="col">VIP</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.filter(appointment => !appointment.cancelled && !appointment.finished).map((appointment, index) => (
                        <tr key={index}>
                            <td>{appointment.customer}</td>
                            <td>{appointment.date_time}</td>
                            <td>{appointment.technician}</td>
                            <td>{appointment.vin}</td>
                            <td>{appointment.reason}</td>
                            <td>
                                <button className="btn btn-warning" onClick={() => cancelAppointment(appointment.id)}>
                                    Cancel
                                </button>
                                <button className="btn btn-success" onClick={() => finishAppointment(appointment.id)}>
                                    Finish
                                </button>
                            </td>
                            <td>{appointment.vip ? 'Yes' : 'No'}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    </>
);

}

export default ListAppointments;