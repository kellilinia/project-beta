import React, { useState, useEffect } from 'react';

function ListVehicleModels() {
    const [models, setModels] = useState([]);

    async function getModels() {
        try {
            const response = await fetch('http://localhost:8100/api/models/');
            if (response.ok) {
                const jsonResponse = await response.json();
                setModels(jsonResponse.models);
            } else {
                console.log('Failed to retrieve models');
            }
        } catch (error) {
            console.log('Error:', error);
        }
    }
    useEffect(() => {
        getModels();
    }
        , []);

    return (
        <>
            <div className="container mt-5">
                <h1>Vehicle Models</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Picture URL</th>
                            <th scope="col">Manufacturer</th>
                        </tr>
                    </thead>
                    <tbody>
                        {models.map((model, index) => (
                            <tr key={index}>
                                <td>{model.name}</td>
                                <td>{model.picture_url}</td>
                                <td>{model.manufacturer.name}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </>
    );
}

export default ListVehicleModels;