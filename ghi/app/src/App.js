import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import CreateTechnician from './CreateTechnician';
import ListTechnicians from './ListTechnicians';
import ListAppointments from './ListAppointments';
import CreateAppointment from './CreateAppointment';
import CreateVehicleModel from './CreateVehicleModel';
import ListVehicleModels from './ListVehicleModels';
import ServiceHistory from './ServiceHistory';
import SalespersonForm from './SalespersonForm';
import SalespeopleList from './SalespeopleList';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import SaleForm from './SaleForm';
import SalesList from './SalesList';
import ManufacturerForm from './ManufacturerForm';
import ManufacturersList from './ManufacturersList';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';
import SalespersonHistory from './SalespersonHistory';
import { useState, useEffect } from 'react';



function App(props) {
  const [salespeople, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [sales, setSales] = useState([]);
  const [manufacturers, setManufacturers] = useState([]);
  const [automobiles, setAutomobiles] = useState([]);

  async function getSalesperson() {
    const response = await fetch('http://localhost:8090/api/salespeople/');
    if (response.ok) {
      const { salespeople } = await response.json();
      setSalespeople(salespeople);
    } else {
      console.error("An error has occured while fetching data")
    }
  }

  async function getCustomer() {
    const response = await fetch('http://localhost:8090/api/customers/');
    if (response.ok) {
      const { customers } = await response.json();
      setCustomers(customers);
    } else {
      console.error("An error has occured while fetching data")
    }
  }

  async function getSale() {
    const response = await fetch('http://localhost:8090/api/sales/');
    if (response.ok) {
      const { sales } = await response.json();
      setSales(sales);
    } else {
      console.error("An error has occured while fetching data")
    }
  }

  async function getManufacturer() {
    const response = await fetch('http://localhost:8100/api/manufacturers/');
    if (response.ok) {
      const { manufacturers } = await response.json();
      setManufacturers(manufacturers);
    } else {
      console.error("An error has occured while fetching data")
    }
  }

  async function getAutomobile() {
    const response = await fetch('http://localhost:8100/api/automobiles/');
    if (response.ok) {
      const { autos } = await response.json();
      setAutomobiles(autos);
    } else {
      console.error("An error has occured while fetching data")
    }
  }

  useEffect(() => {
    getSalesperson();
    getCustomer();
    getSale();
    getManufacturer();
    getAutomobile();
  }, [])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/create-technician" element={<CreateTechnician />} />
          <Route path="/technicians" element={<ListTechnicians />} />
          <Route path="/appointments" element={<ListAppointments />} />
          <Route path="/create-appointment" element={<CreateAppointment />} />"
          <Route path="/service-history" element={<ServiceHistory />} />
          <Route path="salespeople/add" element={< SalespersonForm getSalesperson={getSalesperson} />} />
          <Route path="/salespeople/add" element={< SalespersonForm getSalesperson={getSalesperson} />} />
          <Route path="/salespeople" element={< SalespeopleList salespeople={salespeople} />} />
          <Route path="/customers/add" element={< CustomerForm getCustomer={getCustomer} />} />
          <Route path="/customers" element={< CustomerList customers={customers} />} />
          <Route path="/sales/add" element={< SaleForm getSale={getSale} />} />
          <Route path="/sales" element={< SalesList sales={sales} />} />
          <Route path="/create-vehicle-model" element={<CreateVehicleModel />} />
          <Route path="/models" element={<ListVehicleModels />} />
          <Route path="/salespeople/history" element={< SalespersonHistory salespeople={salespeople} />} />
          <Route path="/manufacturers/add" element={< ManufacturerForm getManufacturer={getManufacturer} />} />
          <Route path="/manufacturers" element={< ManufacturersList manufacturers={manufacturers} />} />
          <Route path="/automobiles/add" element={< AutomobileForm getAutomobile={getAutomobile} />} />
          <Route path="/automobiles" element={< AutomobileList automobiles={automobiles} />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
