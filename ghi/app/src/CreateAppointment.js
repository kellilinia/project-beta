import React, { useState, useEffect } from 'react';

function CreateAppointment() {
    const [appointment, setAppointment] = useState({});
    const [customer, setCustomer] = useState('');
    const [date_time, setDate_time] = useState('');
    const [technician, setTechnician] = useState('');
    const [technicians, setTechnicians] = useState([]);
    const [vin, setVin] = useState('');
    const [reason, setReason] = useState('');

    async function getTechnicians() {
        const response = await fetch('http://localhost:8080/api/technicians/');
        if (response.ok) {
            const { technicians } = await response.json();
            setTechnicians(technicians);
            console.log(technicians);
        } else {
            console.error("An error has occured while fetching data")
        }
    }

    useEffect(() => {
        getTechnicians();
    }, [appointment]);

    const handleCustomerChange = (event) => {
        setCustomer(event.target.value);
    }

    const handleDate_timeChange = (event) => {
        setDate_time(event.target.value);
    }

    const handleTechnicianChange = (event) => {
        setTechnician(event.target.value);
        console.log(event.target.value);
    }

    const handleVinChange = (event) => {
        setVin(event.target.value);
    }

    const handleReasonChange = (event) => {
        setReason(event.target.value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const newAppointment = { customer: customer, date_time: date_time, technician: technician, vin: vin, reason: reason };
        console.log(newAppointment)


        try {
            const response = await fetch('http://localhost:8080/api/appointments/', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(newAppointment)
            });

            if (response.ok) {
                const jsonResponse = await response.json();
                setAppointment(jsonResponse);
            } else {
                console.log('Failed to create appointment');
            }
        }
        catch (error) {
            console.log('Error:', error);
        }
    };


    return (
        <>
            <div className='container'>
                <h1>Create Appointment</h1>
                <form onSubmit={handleSubmit} className='mt-3'>
                    <div className='row'>
                        <div className='col-lg-4'>
                            <div className='form-group'>
                                <label htmlFor='customer'>Customer</label>
                                <input type='text' id='customer' name='customer' className='form-control' value={customer} onChange={handleCustomerChange} />
                            </div>
                            <div className='form-group mt-2'>
                                <label htmlFor='date_time'>Date/Time</label>
                                <input type='datetime-local' id='date_time' name='date_time' className='form-control' value={date_time} onChange={handleDate_timeChange} />
                            </div>
                            <div className='form-group mt-2'>
                                <label htmlFor='technician'>Technician</label>
                                <select value={technician} onChange={handleTechnicianChange} required type='text' id='technician' name='technician' className='form-control'>
                                    {technicians.map((technician, index) => {
                                        return (
                                            <option key={index} value={technician.id}>{technician.first_name} {technician.last_name}</option>
                                        )
                                    })
                                    }
                                </select>
                            </div>
                            <div className='form-group mt-2'>
                                <label htmlFor='vin'>VIN</label>
                                <input value={vin} onChange={handleVinChange} required type='text' id='vin' name='vin' className='form-control' />
                            </div>
                            <div className='form-group mt-2'>
                                <label htmlFor='reason'>Reason</label>
                                <input value={reason} onChange={handleReasonChange} required type='text' id='reason' name='reason' className='form-control' />
                            </div>
                            <div className='form-group mt-2'>
                                <button type='submit' className='btn btn-primary mt-2'>Create Appointment</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </>
    );
}

export default CreateAppointment;
                                    