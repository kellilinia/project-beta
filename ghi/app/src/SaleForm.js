import React, { useEffect, useState } from 'react';



function SaleForm(props) {
    const [price, setPrice] = useState('');
    const [vin, setVin] = useState('');
    const [autos, setAutos] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [salespeople, setSalespeople] = useState([]);
    const [customer, setCustomer] = useState('');
    const [customers, setCustomers] = useState([]);


    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }
    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }
    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }
    const handleSubmit = async (event) => {
        event.preventDefault();


        const data = {};
        data.price = price;
        data.automobile = vin;
        data.salesperson = salesperson;
        data.customer = customer;
        console.log(data);

        const saleUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(saleUrl, fetchConfig);
        if (response.ok) {
            const newSale = await response.json();
            console.log(newSale);
            setPrice('');
            setVin('');
            setSalesperson('');
            setCustomer('');

            props.getSale();
        }
    }

    const fetchAutoData = async () => {
        const url = 'http://localhost:8100/api/automobiles/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos);
        }
    }


    const fetchSalespersonData = async () => {
        const url = 'http://localhost:8090/api/salespeople/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    }


    const fetchCustomerData = async () => {
        const url = 'http://localhost:8090/api/customers/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    }



    useEffect(() => {
        fetchAutoData();
        fetchSalespersonData();
        fetchCustomerData();

    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new Sale</h1>
                    <form onSubmit={handleSubmit} id="create-salerperson-form">
                        <div className="mb-3">
                            <select onChange={handleVinChange} required name="vin" id="vin" value={vin} className="form-select">
                                <option value="">Choose a automobile vin</option>
                                {autos.map(automobile => {
                                    return (
                                        <option key={automobile.vin} value={automobile.vin}>
                                            {automobile.vin}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleSalespersonChange} required name="salesperson" id="salesperson" value={salesperson} className="form-select">
                                <option value="">Choose a salesperson</option>
                                {salespeople.map(salesperson => {
                                    return (
                                        <option key={salesperson.id} value={salesperson.id}>
                                            {salesperson.first_name} {salesperson.last_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleCustomerChange} required name="customer" id="customer" value={customer} className="form-select">
                                <option value="">Choose a customer</option>
                                {customers.map(customer => {
                                    return (
                                        <option key={customer.id} value={customer.id}>
                                            {customer.first_name} {customer.last_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePriceChange} placeholder="Price" required type="price" name="price" id="price" value={price} className="form-control" />
                            <label htmlFor="first_name">Price</label>
                        </div>
                        <button className="btn btn-primary">Create Sale</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default SaleForm;
