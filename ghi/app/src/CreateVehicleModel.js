import React, { useState, useEffect } from 'react';

function CreateVehicleModel() {
    const [model, setModel] = useState({});
    const [name, setName] = useState('');
    const [picture_url, setPicture_url] = useState('');
    const [manufacturer_id, setManufacturer_id] = useState('');
    const [manufacturers, setManufacturers] = useState([]);

    async function getManufacturers() {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
            const { manufacturers } = await response.json();
            setManufacturers(manufacturers);
        } else {
            console.error("An error has occured while fetching data")
        }
    }


    useEffect(() => {
        getManufacturers();
    }, [model]);


    const handleNameChange = (event) => {
        setName(event.target.value);
    }

    const handlePicture_urlChange = (event) => {
        setPicture_url(event.target.value);
    }

    const handleManufacturerChange = (event) => {
        setManufacturer_id(event.target.value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const newModel = { name: name, picture_url: picture_url, manufacturer_id: manufacturer_id };
        
        
        try {
            const response = await fetch('http://localhost:8100/api/models/', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(newModel)
            });
            
            if (response.ok) {
                const jsonResponse = await response.json();
                setModel(jsonResponse);
            } else {
                console.log('Failed to create model');
            }
        } catch (error) {
            console.log('Error:', error);
        }
    };

    return (
        <>
            <div className='container'>
                <h1>Create Vehicle Model</h1>
                <form onSubmit={handleSubmit} className='mt-3'>
                    <div className='row'>
                        <div className='col-lg-4'>
                            <div className='form-group'>
                                <label htmlFor='name'>Name</label>
                                <input type='text' id='name' name='name' className='form-control' value={name} onChange={handleNameChange} />
                            </div>
                            <div className='form-group mt-2'>
                                <label htmlFor='picture_url'>Picture URL</label>
                                <input type='text' id='picture_url' name='picture_url' className='form-control' value={picture_url} onChange={handlePicture_urlChange} />
                            </div>
                            <div className='form-group mt-2'>
                                <label htmlFor='manufacturer'>Manufacturer</label>
                                <select value={manufacturer_id} onChange={handleManufacturerChange} required type='text' id='manufacturer' name='technician' className='form-control'>
                                    {manufacturers.map((manufacturer, index) => {
                                        return (
                                            <option key={index} value={manufacturer.id}>{manufacturer.name}</option>
                                        )
                                    })
                                    }
                                </select>
                            </div>
                            <div className='form-group mt-2'>
                                <button type='submit' className='btn btn-primary mt-2'>Create</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </>
    );

}

export default CreateVehicleModel;