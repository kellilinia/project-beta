import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers">Manufacturers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers/add">Create a Manufacturer</NavLink>
            </li>
            <NavLink className="nav-link" to="/models">Models</NavLink>
            <NavLink className="nav-link" to="/create-vehicle-model">Create a Model</NavLink>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles">Automobiles</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles/add">Create an Automobile</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/salespeople">Salespeople</NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" to="/salespeople/add">Add a Salesperson</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/customers">Customers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/customers/add">Add a Customer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales">Sale</NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" to="/sales/add">Add a Sale</NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" to="/salespeople/history">Sales History</NavLink>
            </li>
            <NavLink className="nav-link" to="/technicians">Technicians</NavLink>
            <NavLink className="nav-link" to="/create-technician">Add a Technician</NavLink>
            <NavLink className="nav-link" to="/service-history">Service Appointments</NavLink>
            <NavLink className="nav-link" to="/create-appointment">Create a Service Appointment</NavLink>
            <NavLink className="nav-link" to="/appointments">Service History</NavLink>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
