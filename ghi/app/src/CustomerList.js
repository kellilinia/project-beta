function CustomerList({ customers }) {
    return (
        <table className="table table-striped">
            <thead>
                <h1>Customers</h1>
                <tr>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Phone Number</th>
                    <th scope="col">Address</th>
                </tr>
            </thead>
            <tbody>
                {customers && customers.map(customer => {
                    return (
                        <tr key={customer.id}>
                            <td>{customer.first_name}</td>
                            <td>{customer.last_name}</td>
                            <td>{customer.phone_number}</td>
                            <td>{customer.address}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    )
}
export default CustomerList
