import React, { useEffect, useState } from 'react';



function AutomobileForm(props) {
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [model, setVehicleModel] = useState('');
    const [models, setVehicleModels] = useState([]);


    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handleyearChange = (event) => {
        const value = event.target.value;
        setYear(value);
    }
    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }
    const handleVehicleModelChange = (event) => {
        const value = event.target.value;
        setVehicleModel(value);
    }
    const handleSubmit = async (event) => {
        event.preventDefault();



        const data = {};
        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = model;
        console.log(data);

        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(automobileUrl, fetchConfig);
        if (response.ok) {
            const newAutomobile = await response.json();
            console.log(newAutomobile);
            setColor('');
            setYear('');
            setVin('');
            setVehicleModel('');

            props.getAutomobile();
        }
    }
    const fetchModelData = async () => {
        const url = 'http://localhost:8100/api/models/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setVehicleModels(data.models);
        }
    }
    useEffect(() => {
        fetchModelData();
    }, []);

    return (
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add an automobile to inventory</h1>
                        <form onSubmit={handleSubmit} id="create-automobile-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleColorChange} placeholder="color" required type="text" name="color" id="color" value={color} className="form-control" />
                                <label htmlFor="color">Color...</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleyearChange} placeholder="year" required type="text" name="year" id="year" value={year} className="form-control" />
                                <label htmlFor="year">Year...</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleVinChange} placeholder="vin" required type="text" name="vin" id="vin" value={vin} className="form-control" />
                                <label htmlFor="vin">VIN...</label>
                            </div>
                            <select onChange={handleVehicleModelChange} required name="model" id="model" value={model} className="form-select">
                                <option value="">Choose a model...</option>
                                {models.map(model => {
                                    return (
                                        <option key={model.id} value={model.name}>
                                            {model.name}
                                        </option>
                                    );
                                })}
                            </select>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}
export default AutomobileForm;
