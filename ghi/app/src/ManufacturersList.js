function ManufacturersList({ manufacturers }) {
    const deleteItem = async (id) => {
        const manufacturersUrl = `http://localhost:8100/api/manufacturers/${id}`
        const fetchConfig = {
            method: "delete",
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(manufacturersUrl, fetchConfig)
        if (response.ok) {
            window.manufacturers.reload()
        }
    }
    return (
        <table className="table table-striped">
            <thead>
                <h1>Manufacturers</h1>
                <tr>
                    <th scope="col">Manufacturer Name</th>
                </tr>
            </thead>
            <tbody>
                {manufacturers && manufacturers.map(manufacturer => {
                    return (
                        <tr key={manufacturer.id}>
                            <td>{manufacturer.name}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    )
}
export default ManufacturersList
