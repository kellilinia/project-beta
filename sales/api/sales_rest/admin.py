from django.contrib import admin
from .models import AutomobileVO, Sale

# Register your models here.

# @admin.register(ManufacturerVO)
# class ManufacturerVOAdmin(admin.ModelAdmin):
#     pass


# @admin.register(VehicleModelVO)
# class VehicleModelVOAdmin(admin.ModelAdmin):
#     pass

@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    pass

@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    pass
