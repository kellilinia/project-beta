from django.db import models
from django.urls import reverse

# Create your models here.
# class ManufacturerVO(models.Model):
#     name = models.CharField(max_length=100, unique=True)


# class VehicleModelVO(models.Model):
#     name = models.CharField(max_length=100)
#     picture_url = models.URLField()
#     manufacturer = models.CharField(max_length=200, null=True)


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200, unique=True)
    sold = models.BooleanField(default=False)



class Salesperson(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200, unique=True)

    def get_api_url(self):
        return reverse("api_show_salesperson", kwargs={"pk": self.id})


class Customer(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=100)

    def get_api_url(self):
        return reverse("api_show_customer", kwargs={"pk": self.id})


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.CASCADE,
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="salesperson",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="customer",
        on_delete=models.CASCADE,
    )
    price = models.CharField(max_length=100)

    def get_api_url(self):
        return reverse("api_show_sale", kwargs={"pk": self.id})
