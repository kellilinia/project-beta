# CarCar

CarCar is an full-stack application that utilizes microservices to manage a car dealership. Inventory,
sales, and services are the three APIs utilized.

## Team

### Kelly Khalilinia - Automobile Sales

### David Francisco - Automobile Services

## GETTING STARTED

**You must have Docker and Git installed.**

1. Go to <https://gitlab.com/kellilinia/project-beta>
and fork this repository. (You must be signed in to do this.)

2. Clone the forked repository onto your local computer.

3. Use the following commands in your repository's root directory:

```DOCKER
docker volume create beta-data
docker-compose build
docker-compose up
```
Verify that your containers are running successfully.

**View the project in the browser at**
<http://localhost:3000/>

## Design

![Img](/ghi/app/public/design.png)

## Inventory Microservice

Inventory is the API that both sales and services relies upon for a variety of features.
It has 3 Models: Automobile, Model, and Manufacturer.

### Manufacturer Model

Fields: name
Use: Manufacturer allows models to be classified together in a one-to-many relationship.

### Model Model

Fields: name, picture_url, manufacturer(foreignKey)
Use: Like manufacturer, Model allows automobiles to be classified together in a one-to-many relationship.

### Automobile Model

Fields: color, year, vin, sold, model(foreignKey)
Use: This is an individual instance of an automobile in an inventory.

### Automobile API Endpoints

When accessing these endpoints, use the following
methods and urls as well as the JSON format. If "vin" or "id" is part of the endpoint, replace that with the VIN (of string type) or id(of integer type) of the target object.

Note:
If a request is sent with a JSON object that does not follow these conventions, an error will occur.

#### GET

List automobiles
<http://localhost:8100/api/automobiles/>

Automobile Detail
<http://localhost:8100/api/automobiles/vin/>

List Models
<http://localhost:8100/api/models/>

List Manufacturers
<http://localhost:8100/api/manufacturers/>

#### POST

Create Automobile
<http://localhost:8100/api/automobiles/>

```JSON
{
  "model_id": 1,
  "vin": "1HGCM82633A123456",
  "color": "blue",
  "year": 2023,
  "sold": false
}
```

Create Model
<http://localhost:8080/api/models/>

```JSON
{
  "manufacturer_id": 1,
  "name": "Model S",
  "picture_url": "http://imagehere.com/model-s.jpg"
}
```

Create Manufacturer
<http://localhost:8080/api/manufacturers/>

```JSON
{
  "name": "Tesla"
}
```

#### PUT

Update Automobile
<http://localhost:8100/api/automobiles/vin/>

```JSON
{
  "color": "red",
  "year": 2024,
  "sold": true
}
```

Update Model
<http://localhost:8100/api/models/id/>

```JSON
{
  "name": "Model S Plaid",
  "picture_url": "http://imagethis.com/model-s-plaid.jpg"
}
```

Update Manufacturer
<http://localhost:8100/api/manufacturers/id/>

```JSON
{
  "name": "X"
}
```

#### DELETE

Delete Automobile
<http://localhost:8100/api/automobiles/vin/>

List Models
<http://localhost:8100/api/models/id/>

List Manufacturers
<http://localhost:8100/api/manufacturers/id/>

## Service microservice

Services consists of two primary pieces, or Models, Technician and Appointment. Using various endpoints,
you can create (hire) technicians, create appointments,
and view service histories of VINs. Using a poller,
services checks the VIN of an automobile being serviced
with the inventory. If the VIN matches, that
customer gets VIP treatment.

### Technician Model

Fields: first_name, last_name, employee_id
Use: Having a technician model will allow me to populate a list of technicians to assign
service appointments.

### AutomobileVO Model

Fields: vin, sold
Use: The value object instance of the automobile will allow me to input cars with their vin
and assign technicians to the appointment. I can also use vin and sold fields to assign vip boolean.

### Appointment Model

Fields: date_time, reason, status, vin, customer, technician(foreign key)
Use: Enables users to create an appointment and appointments get stored.

### Service API Endpoints

#### GET

List service appointments
<http://localhost:8080/api/listappointments/>

Service Appointment By ID
<http://localhost:8080/api/listappointments/int:id>

List Technicians
<http://localhost:8080/api/technicians/>

#### POST

Create Technician
<http://localhost:8080/api/technians/>

```JSON
{
    "first_name": "David",
    "last_name": "Francisco",
    "employee_id": "EEE12CB"
}
```

Create Appointment
<http://localhost:8080/api/appointments>

```JSON
{
  "technician": 1,
  "vin": "yyy389uvjviosdc",
  "date_time": "2023-07-28T15:30:00",
  "reason": "Rosè Refill",
  "customer": "Rick Ross"
}
```


#### PUT



#### DELETE

Delete Technician
<http://localhost:8080/api/technicians/int:id>

### Services Poller

The services poller polls the automobile API for the current inventory. It creates a database of value object instances for the services API to interact with, instead of directly with the inventory API. This separates them, meaning that errors that happen in the services API won't interfere with the Inventory API, since there's no direct relationship.













## Sales microservice

The sales microservice is composed of three primary elements, the Salesperson, Customer, and Sale (the specific data that are of interest can be viewed in the sales_rest/models.py file). The end-points related to the models allow you to:
    - Add a salesperson
    - View the list of all salespeople
    - Add a customer
    - View the list of all customers
    - Add a sale
    - View sales history


### Salesperson Model
The Salesperson Model consists of the following fields:
    - first_name
    - last_name
    - employe_id

Use: The Salesperson Model is used to create the browser views for the salesperson form, the list of salespeople, and the sales history of a salesperson. Additionally, the salesperson information is extracted and fed into the sale form and sales list views.

### Customer Model
The Customer Model consists of the following fields:
    - first_name
    - last_name
    - employee_id

Use: The Customer Model is used to create the browser views for the customer form, and the list of customers. Additionally, the customer information is extracted and fed into the sale form and sales list, and sales history views as well as for part of the service microservice views.

### Sale Model
The Sale Model consists of the following fields:
    - first_name
    - last_name
    - address
    - phone_number

Use: The Sales Model is used to create the browser views for the sale form, and sales list, and is additionally used for the VIP in the service microservice views.

### AutomobileVO Model
The automobile Model consists of the following fields and is the value object that the poller updates from the Automobile Model in the inventory microservice:
    - vin
    - sold (true or false)

Use: The AutomobileVO Model is used to provide the vin and sold fields required for the sales form and list views, and sales history, which is essentially updated from the poller (the bridge between the inventory microservice Automobile Model and the Automobile Value Object Model in the sales microservice)




### Sales API Endpoints

#### GET

List salesperson
<http://localhost:8090/api/salespeople/>

Salesperson details by ID
<http://localhost:8090/api/salespeople/int:id/>

List Customers
<http://localhost:8080/api/technicians/>

Customer details by ID
<http://localhost:8090/api/customers/int:id/>

List sales
<http://localhost:8090/api/sales/>

Sale details by ID
<http://localhost:8090/api/salespeople/int:id/>


#### POST

Create salesperson
<http://localhost:8090/api/salespeople/>

```JSON
{
	"first_name": "John",
	"last_name": "Doe",
	"employee_id": "1"
}
```


Create customer
<http://localhost:8090/api/customers/>

```JSON
{
	"first_name": "Mindy",
	"last_name": "Thompson",
	"address": "1313 Main Street, San Francisco CA 94551",
	"phone_number": "909-867-5309"
}
```


Create sale
<http://localhost:8090/api/sales/>

```JSON
{
	"id": 1,
	"price": 29100,
	"salesperson": 1,
	"customer": 1,
    "automobile": "TLY123HRSDHT5436456"
}
```

#### PUT

Update salesperson
<http://localhost:8090/api/salespeople/int:id/>
```JSON
{
	"first_name": "Jonathan",
	"last_name": "Doe",
	"employee_id": "1"
}
```

Update customer

```JSON
{
	"first_name": "Miranda",
	"last_name": "Thompson",
	"address": "1313 Main Street, San Francisco CA 94551",
	"phone_number": "909-867-5309"
}
```


Update sale
<http://localhost:8090/api/sales/int:id/>

```JSON
{
	"price": 28150

}
```



#### DELETE

Delete salesperson
<http://localhost:8090/api/salespeople/int:id/>

```JSON
{
    "deleted": true
}
```

Delete customer
<http://localhost:8090/api/customers/int:id/>

```JSON
{
    "deleted": true
}
```

Delete sale
<http://localhost:8090/api/sales/int:id/>

```JSON
{
    "deleted": true
}
```


### Sales Poller
The sales poller polls from the automobile API to transfer information added to the inventory API (the inventory api stores information surrounding the manufacturer, model, and automobiles); this is the bridge between the sales microservice and the inventory microservice so the vin and sold field is updated and provided to the sales microservice (polled every 60 seconds).
